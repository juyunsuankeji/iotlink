package com.yunze.apiCommon.upstreamAPI.YiDongEC.Inquire;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.yunze.apiCommon.upstreamAPI.YiDongEC.YD_EC_Api;
import com.yunze.apiCommon.utils.HttpUtil;
import com.yunze.apiCommon.utils.UrlUtil;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 查询类接口
 */
public class Query_YD extends YD_EC_Api {



    public Query_YD() {
        super();
    }

    public Query_YD(Map<String, Object> init_map, String cd_code) {
        super(init_map,cd_code);
    }






    /**
     * 单卡本月套餐内流量使用量实时查询
     * @param card_no
     * @return
     */
    public String queryGprsMarginBySim(String card_no) {
        String functionNm =   "/query/sim-data-margin";
        JSONObject retult = new JSONObject();
        JSONObject json = new JSONObject();
        if(!cd_code.equals("YiDong_EC_TengYu")){
            json.put("transid", appId + new SimpleDateFormat("YYYYMMDDHHMMSS").format(new Date()));
            json.put("token", token);
            json.put("msisdn", card_no);
        }else{
            json.put("interface", functionNm);
            json.put("appid", appId);
            Map<String,Object> requestParams = new HashMap<>();
            requestParams.put("msisdn",card_no);
            json.put("requestParams", requestParams);
        }
        String url = server_Ip + functionNm;
        String Str = send(url,json);
        return repeat(Str,json,url);
    }



    /**
     * 单卡接口查询卡状态
     * @param card_no
     * @return
     */
    public String queryCardStatus(String card_no)  {
        String functionNm =   "/query/sim-status";

        JSONObject json = new JSONObject();
        String cardStatus = null;
        String lastChangeDate = null;
        String url = server_Ip + functionNm;
        if(!cd_code.equals("YiDong_EC_TengYu")){
            json.put("transid", appId + new SimpleDateFormat("YYYYMMDDHHMMSS").format(new Date()));
            json.put("token", token);
        }else{
            json.put("interface", functionNm);
            json.put("appid", appId);
            Map<String,Object> requestParams = new HashMap<>();
            requestParams.put("msisdn",card_no);
            json.put("requestParams", requestParams);
        }
        json.put("msisdn", card_no);
        String Str = send(url,json);
        return repeat(Str,json,url);
    }

    /**
     * 月流量查询接口
     * @param card_no
     * @return
     */
    public String selectFlow(String card_no) {
        String functionNm =   "/query/sim-data-usage";
        JSONObject retult = new JSONObject();
        JSONObject json = new JSONObject();
        if(!cd_code.equals("YiDong_EC_TengYu")){
            json.put("transid", appId + new SimpleDateFormat("YYYYMMDDHHMMSS").format(new Date()));
            json.put("token", token);
            json.put("msisdn", card_no);
        }else{
            json.put("interface", functionNm);
            json.put("appid", appId);
            Map<String,Object> requestParams = new HashMap<>();
            requestParams.put("msisdn",card_no);
            json.put("requestParams", requestParams);
        }
        String url = server_Ip + functionNm;
        String Str = send(url,json);
        //System.out.println(Str);
        return repeat(Str,json,url);
    }

    /**
     * 查询指定月份的已用流量。6个月内的
     *
     * @param card_no
     * @param queryDate
     * @return
     */
    public String designatedMonth(String card_no, String queryDate) {
        String functionNm =  "/query/sim-data-usage-monthly/batch";
        JSONObject retult = new JSONObject();
        JSONObject json = new JSONObject();
        if(!cd_code.equals("YiDong_EC_TengYu")){
            json.put("transid", appId + new SimpleDateFormat("YYYYMMDDHHMMSS").format(new Date()));
            json.put("token", token);
            json.put("msisdns", card_no);
        }else{
            json.put("interface", functionNm);
            json.put("appid", appId);
            Map<String,Object> requestParams = new HashMap<>();
            requestParams.put("msisdn",card_no);
            json.put("requestParams", requestParams);
        }

        json.put("queryDate", queryDate);
        String url = server_Ip + functionNm;
        String Str = send(url,json);
        return repeat(Str,json,url);
    }


    /**
     * 单卡卡状态操作
     * @param card_no
     * @param flag
     * @return
     */
    /*public String changeCardStatus(String card_no, String flag) {
        String functionNm =  "/change/sim-status";
        JSONObject retult = new JSONObject();
        JSONObject json = new JSONObject();
        String url = server_Ip + functionNm;
        if(!cd_code.equals("YiDong_EC_TengYu")){
            json.put("transid", appId + new SimpleDateFormat("YYYYMMDDHHMMSS").format(new Date()));
            json.put("token", token);
            json.put("msisdn", card_no);
        }else{
            json.put("interface", functionNm);
            json.put("appid", appId);
            Map<String,Object> requestParams = new HashMap<>();
            requestParams.put("msisdn",card_no);
            json.put("requestParams", requestParams);
        }
        json.put("operType", flag);
        String Str = send(url,json);
        return repeat(Str,json,url);
    }*/

    /**
     * 查询单卡的机卡分离状态
     * @param card_no
     * @return
     */
    public String queryCardBindStatus(String card_no) {
        String functionNm =  "/query/card-bind-status";
        JSONObject retult = new JSONObject();
        JSONObject json = new JSONObject();
        String cardStatus = null;
        String sepTime = null;
        String url = server_Ip + functionNm;
        if(!cd_code.equals("YiDong_EC_TengYu")){
            json.put("transid", appId + new SimpleDateFormat("YYYYMMDDHHMMSS").format(new Date()));
            json.put("token", token);
            json.put("msisdn", card_no);
        }else{
            json.put("interface", functionNm);
            json.put("appid", appId);
            Map<String,Object> requestParams = new HashMap<>();
            requestParams.put("msisdn",card_no);
            json.put("requestParams", requestParams);
        }

        json.put("testType", 0);
        String Str = send(url,json);
        return repeat(Str,json,url);
    }

    /**
     * 解除单卡的机卡分离
     * @param card_no
     * @return
     */
   /* public String upCardBindStatus(String card_no) {
        String functionNm =  "/operate/card-bind-by-bill";
        JSONObject retult = new JSONObject();
        JSONObject json = new JSONObject();
        String url = server_Ip + functionNm;
        if(!cd_code.equals("YiDong_EC_TengYu")){
            json.put("transid", appId + new SimpleDateFormat("YYYYMMDDHHMMSS").format(new Date()));
            json.put("token", token);
            json.put("msisdn", card_no);
        }else{
            json.put("interface", functionNm);
            json.put("appid", appId);
            Map<String,Object> requestParams = new HashMap<>();
            requestParams.put("msisdn",card_no);
            json.put("requestParams", requestParams);
        }

        json.put("operType", 2);
        String Str = send(url,json);
        return repeat(Str,json,url);
    }*/

    /**
     * 单卡查询激活以及开卡时间
     * @param card_no
     * @return
     */
    public String queryCardActiveTime(String card_no) {
        String functionNm =  "/query/sim-basic-info";
        JSONObject retult = new JSONObject();
        JSONObject json = new JSONObject();
        String url = server_Ip + functionNm;
        if(!cd_code.equals("YiDong_EC_TengYu")){
            json.put("transid", appId + new SimpleDateFormat("YYYYMMDDHHMMSS").format(new Date()));
            json.put("token", token);
            json.put("msisdn", card_no);
        }else{
            json.put("interface", functionNm);
            json.put("appid", appId);
            Map<String,Object> requestParams = new HashMap<>();
            requestParams.put("msisdn",card_no);
            json.put("requestParams", requestParams);
        }

        String Str = send(url,json);
        return repeat(Str,json,url);
    }

    /**
     * @param @param  card_no
     * @param @return 参数
     * @return JSONObject    返回类型
     * @throws
     * @Description:单卡查询apn状态
     * @author
     * @date 2020年5月25日
     */
    public  Map<String, Object> queryApnStatus(String card_no) {
        String functionNm =  "/query/sim-communication-function-status";
        JSONObject json = new JSONObject();
        String url = server_Ip + functionNm;
        if(!cd_code.equals("YiDong_EC_TengYu")){
            json.put("transid", appId + new SimpleDateFormat("YYYYMMDDHHMMSS").format(new Date()));
            json.put("token", token);
            json.put("msisdn", card_no);
        }else{
            json.put("interface", functionNm);
            json.put("appid", appId);
            Map<String,Object> requestParams = new HashMap<>();
            requestParams.put("msisdn",card_no);
            json.put("requestParams", requestParams);
        }

        //String Str = HttpUtil.post(url, json.toJSONString());
        String Str = send(url,json);
        return JSONObject.parseObject(repeat(Str,json,url));
    }

    /**
     *  流量池信息查询
     * @return
     */
    public static String queryFlowPool(){
        String functionNm =  "/query/group-data-margin";
        JSONObject json = new JSONObject();
        String url = server_Ip + functionNm;
        if(!cd_code.equals("YiDong_EC_TengYu")){
            json.put("transid", appId + new SimpleDateFormat("YYYYMMDDHHMMSS").format(new Date()));
            json.put("token", token);
        }
        json.put("groupId", appId);
        String Str = HttpUtil.post(url, json.toJSONString());

        return Str;

    }


    /**
     * 单卡在线信息实时查询
     * @param card_no
     * @return
     */
    public String sim_session(String card_no) {
        String functionNm =  "/query/sim-session";
        JSONObject json = new JSONObject();
        String cardStatus = null;
        String lastChangeDate = null;
        String url = server_Ip + functionNm;
        if(!cd_code.equals("YiDong_EC_TengYu")){
            json.put("transid", appId + new SimpleDateFormat("YYYYMMDDHHMMSS").format(new Date()));
            json.put("token", token);
            json.put("msisdn", card_no);
        }else{
            json.put("interface", functionNm);
            json.put("appid", appId);
            Map<String,Object> requestParams = new HashMap<>();
            requestParams.put("msisdn",card_no);
            json.put("requestParams", requestParams);
        }

        String Str = send(url,json);
        return repeat(Str,json,url);
    }


    /**
     * 单卡实时使用终端 IMEI 查询
     * @param card_no
     * @return
     */
    public String queryCardImei(String card_no) {
        String functionNm =  "/query/sim-imei";
        JSONObject json = new JSONObject();
        String cardStatus = null;
        String lastChangeDate = null;
        String url = server_Ip + functionNm;
        if(!cd_code.equals("YiDong_EC_TengYu")){
            json.put("transid", appId + new SimpleDateFormat("YYYYMMDDHHMMSS").format(new Date()));
            json.put("token", token);
            json.put("msisdn", card_no);
        }else{
            json.put("interface", functionNm);
            json.put("appid", appId);
            Map<String,Object> requestParams = new HashMap<>();
            requestParams.put("msisdn",card_no);
            json.put("requestParams", requestParams);
        }
        String Str = send(url,json);
        return repeat(Str,json,url);
    }



    /**
     * 单卡停机原因查询
     * @param card_no
     * @return
     */
    public String simStopReason(String card_no) {
        String functionNm =  "/query/sim-stop-reason";
        JSONObject json = new JSONObject();
        String url = server_Ip + functionNm;
        if(!cd_code.equals("YiDong_EC_TengYu")){
            json.put("transid", appId + new SimpleDateFormat("YYYYMMDDHHMMSS").format(new Date()));
            json.put("token", token);
            json.put("msisdn", card_no);
        }else{
            json.put("interface", functionNm);
            json.put("appid", appId);
            Map<String,Object> requestParams = new HashMap<>();
            requestParams.put("msisdn",card_no);
            json.put("requestParams", requestParams);
        }
        String Str = send(url,json);
        return repeat(Str,json,url);
    }




    /**
     * 单卡开关机状态实时查询
     * @param card_no
     * @return
     */
    public String onOffStatus(String card_no) {
        String functionNm =  "/query/on-off-status";
        JSONObject json = new JSONObject();
        String url = server_Ip + functionNm;
        if(!cd_code.equals("YiDong_EC_TengYu")){
            json.put("transid", appId + new SimpleDateFormat("YYYYMMDDHHMMSS").format(new Date()));
            json.put("token", token);
            json.put("msisdn", card_no);
        }else{
            json.put("interface", functionNm);
            json.put("appid", appId);
            Map<String,Object> requestParams = new HashMap<>();
            requestParams.put("msisdn",card_no);
            json.put("requestParams", requestParams);
        }
        String Str = send(url,json);
        return repeat(Str,json,url);
    }

    /**
     * 单卡已开通APN信息查询
     * @param card_no
     * @return
     */
    public String apnInfo(String card_no) {
        String functionNm =  "/query/apn-info";
        JSONObject json = new JSONObject();
        String url = server_Ip + functionNm;
        if(!cd_code.equals("YiDong_EC_TengYu")){
            json.put("transid", appId + new SimpleDateFormat("YYYYMMDDHHMMSS").format(new Date()));
            json.put("token", token);
            json.put("msisdn", card_no);
        }else{
            json.put("interface", functionNm);
            json.put("appid", appId);
            Map<String,Object> requestParams = new HashMap<>();
            requestParams.put("msisdn",card_no);
            json.put("requestParams", requestParams);
        }
        String Str = send(url,json);
        return repeat(Str,json,url);
    }


    /**
     * 物联卡机卡分离状态查询
     * @param card_no
     * @param testType 分离检测方式：
     * 0：话单侧检测
     * 1：网络侧检测
     * @return
     */
    public String cardBindStatus(String card_no,String testType) {
        String functionNm =  "/query/card-bind-status";
        JSONObject json = new JSONObject();
        String url = server_Ip + functionNm;
        if(!cd_code.equals("YiDong_EC_TengYu")){
            json.put("transid", appId + new SimpleDateFormat("YYYYMMDDHHMMSS").format(new Date()));
            json.put("token", token);
            json.put("msisdn", card_no);
            json.put("testType", testType);
        }else{
            json.put("interface", functionNm);
            json.put("appid", appId);
            json.put("testType", testType);
            Map<String,Object> requestParams = new HashMap<>();
            requestParams.put("msisdn",card_no);
            json.put("requestParams", requestParams);
        }
        String Str = send(url,json);
        return repeat(Str,json,url);
    }



    /**
     * 单卡状态变更历史查询
     * @param card_no
     * @return
     */
    public String simChangeHistory(String card_no) {
        String functionNm =  "/query/sim-change-history";
        JSONObject json = new JSONObject();
        String url = server_Ip + functionNm;
        if(!cd_code.equals("YiDong_EC_TengYu")){
            json.put("transid", appId + new SimpleDateFormat("YYYYMMDDHHMMSS").format(new Date()));
            json.put("token", token);
            json.put("msisdn", card_no);
        }else{
            json.put("interface", functionNm);
            json.put("appid", appId);
            Map<String,Object> requestParams = new HashMap<>();
            requestParams.put("msisdn",card_no);
            json.put("requestParams", requestParams);
        }
        String Str = send(url,json);
        return repeat(Str,json,url);
    }








    /**
     * 单卡本月套餐内流量使用量实时查
     * @param card_no
     * @return
     */
    public String simDataMargin(String card_no) {
        String functionNm =  "/query/sim-data-margin";
        JSONObject json = new JSONObject();
        String url = server_Ip + functionNm;
        if(!cd_code.equals("YiDong_EC_TengYu")){
            json.put("transid", appId + new SimpleDateFormat("YYYYMMDDHHMMSS").format(new Date()));
            json.put("token", token);
            json.put("msisdn", card_no);
        }else{
            json.put("interface", functionNm);
            json.put("appid", appId);
            Map<String,Object> requestParams = new HashMap<>();
            requestParams.put("msisdn",card_no);
            json.put("requestParams", requestParams);
        }
        String Str = send(url,json);
        return repeat(Str,json,url);
    }



    /**
     * 资费订购实时查询
     * @param card_no
     * @return
     */
    public String queryOffering(String card_no) {
        String functionNm =  "/query/ordered-offerings";
        JSONObject json = new JSONObject();
        String url = server_Ip + functionNm;
        if(!cd_code.equals("YiDong_EC_TengYu")){
            json.put("transid", appId + new SimpleDateFormat("YYYYMMDDHHMMSS").format(new Date()));
            json.put("token", token);
            json.put("msisdn", card_no);
            json.put("queryType", "3");
        }else{
            json.put("interface", functionNm);
            json.put("appid", appId);
            json.put("queryType", "3");
            Map<String,Object> requestParams = new HashMap<>();
            requestParams.put("msisdn",card_no);
            json.put("requestParams", requestParams);
        }
        String Str = send(url,json);
        return repeat(Str,json,url);
    }


    /**
     * CMIOT_API23E06-集团群组信息查询
     * @param pageSize 每页查询的数目，不超过50
     * @param startNum 开始页，从1开始
     * @return
     */
    public String queryGroupInfo(String pageSize,String startNum) {
        String functionNm =  "/query/group-info";
        JSONObject json = new JSONObject();
        String url = server_Ip + functionNm;
        if(!cd_code.equals("YiDong_EC_TengYu")){
            json.put("transid", appId + new SimpleDateFormat("YYYYMMDDHHMMSS").format(new Date()));
            json.put("token", token);
            json.put("pageSize", pageSize);
            json.put("startNum", startNum);
        }else{
            json.put("interface", functionNm);
            json.put("appid", appId);
            json.put("pageSize", pageSize);
            json.put("startNum", startNum);
            Map<String,Object> requestParams = new HashMap<>();
            json.put("requestParams", requestParams);
        }
        String Str = send(url,json);
        return repeat(Str,json,url);
    }

    /**
     * CMIOT_API23E01-群组所属成员查询
     * @param pageSize 每页查询的数目，不超过50
     * @param startNum 开始页，从1开始
     * @param groupId 群组ID
     * @return
     */
    public String queryGroupMember(String pageSize,String startNum,String groupId) {
        String functionNm =  "/query/group-member";
        JSONObject json = new JSONObject();
        String url = server_Ip + functionNm;
        if(!cd_code.equals("YiDong_EC_TengYu")){
            json.put("transid", appId + new SimpleDateFormat("YYYYMMDDHHMMSS").format(new Date()));
            json.put("token", token);
            json.put("groupId", groupId);
            json.put("pageSize", pageSize);
            json.put("startNum", startNum);
        }else{
            json.put("interface", functionNm);
            json.put("appid", appId);
            json.put("groupId", groupId);
            json.put("pageSize", pageSize);
            json.put("startNum", startNum);
            Map<String,Object> requestParams = new HashMap<>();
            json.put("requestParams", requestParams);
        }
        String Str = send(url,json);
        return repeat(Str,json,url);
    }


}
