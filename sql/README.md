## sql 文件夹说明


### 命名规则

> 文件名以 ‘iotdb’ 开头 + 版本号 + 更新日期 如 【iotdb0.5.0-20220112.sql】 为 0.5.0 版本 更新 时间 为 2022-01-12

### 必须执行 SQL queryChildrenAreaInfo.sql

> 'queryChildrenAreaInfo.sql' 为 查询企业下子企业 查询函数 用来控制权限管理 执行该 SQL 需要 root 账号去执行 

### 如果您是首次部署 IoTLink 

> 如果您是首次部署 IoTLink 请直接执行 最新版本 一键一直版 SQL


### 如果您需要部署 微信端

> 如果您需要部署 微信端 功能 需要 执行 【iotdb0.7.0-202203331-wxWeb.sql】 SQL 文件。
