CREATE TABLE `yz_agent_account` (
                                    `id` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
                                    `app_id` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '唯一识别',
                                    `access_key` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '密钥',
                                    `password` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '密码',
                                    `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                                    `update_time` datetime DEFAULT NULL COMMENT '更新时间',
                                    `agent_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户名',
                                    `agent_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '关联企业 agent_id',
                                    `times` int(11) DEFAULT NULL COMMENT '调用频率',
                                    `openurl` varchar(20000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '放行接口',
                                    `typesys` int(11) NOT NULL DEFAULT '0' COMMENT '是否为系统内部账户',
                                    PRIMARY KEY (`id`) USING BTREE,
                                    UNIQUE KEY `id` (`id`) USING BTREE,
                                    UNIQUE KEY `app_id` (`app_id`) USING BTREE,
                                    KEY `agent_id` (`agent_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='企业调用api账户';