package com.yunze.iotapi.controller.MyEC;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yunze.apiCommon.mapper.YzCardRouteMapper;
import com.yunze.apiCommon.upstreamAPI.YiDongEC.ResJson;
import com.yunze.iotapi.entity.AgentAccount;
import com.yunze.iotapi.service.impl.AgentAccountServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  适配返回移动官方数据
 */
@Controller
@RequestMapping("/v5/ec")
public class ECAapiController {

    @Resource
    private YzCardRouteMapper yzCardRouteMapper;

    @Resource
    private AgentAccountServiceImpl agentAccountService;


    /**
     * 获取token
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/get/token")
    @ResponseBody
    public JSONObject getToken(HttpServletRequest request, HttpServletResponse response) {

        String appid = request.getParameter("appid").toString();
        String password = request.getParameter("password").toString();
        String transid = request.getParameter("transid").toString();
        List<JSONObject> result = new ArrayList<JSONObject>();
        JSONObject jobj =  new JSONObject();
        try {

            AgentAccount agentAccount=agentAccountService.getOne(new QueryWrapper<AgentAccount>().eq("app_id",appid).eq("password",password).select("times"));
            if(null!=agentAccount){
                jobj.put("token",agentAccount.getTimes());
            }else{
                jobj.put("token","-1");
                result.add(jobj);
                return new ResJson("12022","PASSWORD 鉴权不通过",result);
            }
            result.add(jobj);
            return new ResJson().success(result);
        } catch (Exception e) {
            jobj.put("token","-1");
            result.add(jobj);
            return new ResJson("12022","PASSWORD 鉴权不通过",result);
        }
    }


    /**
     * 单卡本月流量累计使用量查询
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/query/sim-data-usage")
    @ResponseBody
    public JSONObject simDataUsage(HttpServletRequest request, HttpServletResponse response) {
        String msisdn = request.getParameter("msisdn").toString();
        List<JSONObject> result = new ArrayList<JSONObject>();
        JSONObject jobj =  new JSONObject();
        try {
            Map<String,Object> map = new HashMap<String,Object>();
            map.put("iccid",msisdn);
            String used = yzCardRouteMapper.find_Out_used(map);
            if(null!=used){
                Double Mb = Double.parseDouble(used);
                Mb  = Mb*1024;
                jobj.put("dataAmount",Mb);
            }else{
                return new ResJson("11007","查询号码全部非法，请确认号码正确性",result);
            }
            result.add(jobj);
            return new ResJson().success(result);
        } catch (Exception e) {
            jobj.put("token","-1");
            result.add(jobj);
            return new ResJson("12022","PASSWORD 鉴权不通过",result);
        }
    }







}

