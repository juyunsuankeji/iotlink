package com.yunze.iotapi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yunze.iotapi.entity.SysLogs;
import com.yunze.iotapi.mapper.SysLogsMapper;
import com.yunze.iotapi.service.SysLogsService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 系统日志 业务实现类
 */
@Service
public class SysLogsServiceImpl extends ServiceImpl<SysLogsMapper, SysLogs> implements SysLogsService {

    @Resource
    private SysLogsMapper sysLogsMapper;




}
