package com.yunze.iotapi.config;


import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.Connection;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

@Component
public class RabbitMQConfig {

    public Connection connection ;

    @Resource
    private RabbitTemplate rabbitTemplate;

    public Integer getCount(String EXCHANGE_NAME,String queue,String Key,String Type) throws IOException {
        Integer queueCount = null;
        ConnectionFactory connectionFactory = rabbitTemplate.getConnectionFactory();
        try {
            connection = connectionFactory.createConnection();
            Channel channel = connection.createChannel(false);
            // 通道关联交换机
            channel.exchangeDeclare(EXCHANGE_NAME, Type);
            AMQP.Queue.DeclareOk declareOk = channel.queueDeclarePassive(queue);//直连交换机类型
            //获取队列中的消息个数
            queueCount = declareOk.getMessageCount();
            channel.close();
        }catch (Exception e){
            System.out.println("===== creatExchangeQueue connection.getCount() [Start] =====");
            System.out.println(e.getMessage());
            System.out.println("===== creatExchangeQueue connection.getCount() [End] =====");
        }
        finally {
            connection.close();
            connection = null;
        }
        return queueCount;
    }




    /**
     * 获取连接 对象
     * @return
     */
    public Connection getConnection(){
        try {
            ConnectionFactory connectionFactory = rabbitTemplate.getConnectionFactory();
            connection = connectionFactory.createConnection();
        }catch (Exception e){
            System.out.println("getConnection Exception "+e.getMessage());
        }
        return  connection;
    }




    /**
     * 手动发送消息
     * @param EXCHANGE_NAME 交换机
     * @param queue 队列
     * @param Key 网线
     * @param Type 交换机类型
     * @param msg 发送消息
     * @throws IOException
     * @throws TimeoutException
     */
    public void send(String EXCHANGE_NAME,String queue,String Key,String Type,String msg) throws IOException, TimeoutException {
        // 创建Channel
        ConnectionFactory connectionFactory = rabbitTemplate.getConnectionFactory();
        connection = connectionFactory.createConnection();
        try {
            BindingBuilder.bind(new Queue(queue)).to(new DirectExchange(EXCHANGE_NAME)).with(Key);// 队列 绑定 交换机
            Channel channel = connection.createChannel(false);
            // 通道关联交换机
            channel.exchangeDeclare(EXCHANGE_NAME, Type, true);
            channel.basicPublish(EXCHANGE_NAME, Key, null, msg.getBytes("utf-8"));
            channel.close();
        } catch (Exception e) {
            System.out.println("===== creatExchangeQueue connection.createChannel() [Start] =====");
            System.out.println(e.getMessage());
            System.out.println("===== creatExchangeQueue connection.createChannel() [End] =====");
        } finally {
            connection.close();
            connection = null;
        }
    }



}