package com.yunze.iotapi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yunze.iotapi.entity.SysLogs;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统日志 Mapper
 */
@Mapper
public interface SysLogsMapper extends BaseMapper<SysLogs> {


}
